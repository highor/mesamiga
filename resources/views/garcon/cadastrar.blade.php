@extends ('layouts.layout')
@section ('template')
    <header>
        <div class="cabecalho">
            <a href="/"><img src="{{ asset('imagens/logo.png')}}"></a>
        </div>
    </header>
    <div class="cadastros" align="center">
        <div class="elemento">
            <h3>Cadastrar Garçon</h3>
        </div>  
        <form name="inserir_garcon" method="POST" action="{{ url('/garcon/inserir') }}">
            {{ csrf_field() }}
            <div class="elemento">
                <input type="text" name="NOME" placeholder="Nome do garçon" required>
            </div>
            <div class="elemento">
        	   <button type="submit">Inserir Garçon</button>
            </div>
        </form>
    </div>
@endsection