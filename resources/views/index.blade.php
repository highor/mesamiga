@extends ('layouts.layout')
@section ('template')
    <header>
        <div class="cabecalho">
            <a href="/"><img src="{{ asset('imagens/logo.png')}}"></a>
            <div>
                <fieldset class="horarios">
                    <legend>Almoço:</legend>
                    <div class="elemento">
                        <label>Segundas à Sextas, das 11:30 às 14:00 horas.</label>
                    </div>
                    <div class="elemento">
                        <label>Sábados e Domingos, das 11:30 às 14:30 horas.</label>    
                    </div>
                </fieldset>

                <fieldset class="horarios">
                    <legend>Jantar:</legend>
                    <div class="elemento">
                        <label>Terças à Sábados, à partir das 18:00.</label>
                    </div>                    
                </fieldset>

                <div class="localizacao" align="center">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3575.3621315032005!2d-48.84871668547555!3d-26.347146776197484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94deb0da4d1074f7%3A0x660c8face560a8b!2sRestaurante+Virado+no+Alho!5e0!3m2!1spt-BR!2sbr!4v1508521104063" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="menu_principal">
            <nav> 
                <ul>
                    <li><a class="botao_menu" href="{{url('/mesas')}}">Mesas</a></li>
                    <li><a class="botao_menu" href="{{url('/cadastros')}}">Cadastros</a></li>
                </ul>
            </nav>
        </div>
        </header>

        <div id="corpo" class="corpo" align="center">
            <div class="conteudo">
                @yield('conteudo')
            </div>
        </div>
    @include('layouts.resultados')
@endsection
