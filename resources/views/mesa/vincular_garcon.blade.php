@extends ('layouts.layout')
@section ('template')
<div id="modal" class="modal" align="center">
	<div class="conteudo_modal">
		<label>Informe o garçon da mesa {{$mesa}}</label>
		<form id="confirmar_garcon" name="confirmar_garcon" method="POST" action="{{ url('/vincular/garcon', $mesa) }}">
		{{ csrf_field() }}
		<br>
			<div class="elemento">
				<select name="NR_GARCON" data-validate="required">
	                <option value="" selected>selecione um garcon</option>
	                @foreach($garcons as $garcon)
	                    <option value="{{ $garcon->NR_GARCON }}">{{ $garcon->NOME }}</option>
	                @endforeach
	            </select>
            </div>
			<br>
            <div class="elemento">
            	<button type="submit">Vincular garçon</button>
            </div>
        </form>
	</div>
</div>
@endsection