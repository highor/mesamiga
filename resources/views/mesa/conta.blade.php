@extends ('index')
@section ('conteudo')
	@foreach($contas_mesa_atrasadas as $conta)
		@if($conta->DATA != date("Y-m-d") && $conta->HORA_FECHAMENTO == null)
		<div class="pedido_mesa">
			<div class="elemento">
				<h3>Conta Atrasada</h3>
			</div>
			<div class="pedido_em_debito">
				<table cellspacing="0" cellpadding="2" border="1" class="table-relatorio">
					<thead>
						<tr>
							<th>Data</th>
							<th>Hora de Abertura</th>
							<th>Item do Pedido</th>
							<th>Quantidade</th>
							<th>Valor</th>
							<th>Total</th>
							<th>Garçon</th>
						</tr>
					</thead>
					<tbody  class="relatorio">
						@foreach($conta->pedido as $pedido)
							@foreach($pedido->itemCardapio as $item)
							<tr class="tdContas">
								<td>
									<input type="hidden" name="NR_CONTA" value="{{ $conta->NR_CONTA }}">
									{{ date('d/m/Y', strtotime($conta->DATA)) }}
								</td>
								<td>
									{{ $conta->HORA_ABERTURA }}
								</td>
								<td>
									{{ $item->NOME }}
								</td>
								<td>
									{{ $pedido->QUANTIDADE }}
								</td>
								<td>
									R$ {{ number_format($pedido->PRECO_UNITARIO, 2, ',','') }}
								</td>
								<td>
									R$ {{ number_format($pedido->QUANTIDADE * $pedido->PRECO_UNITARIO, 2, ',','' )}}									
								</td>
								<td>
									{{ $conta->garcon->NOME }}
								</td>
							</tr>
							@endforeach
						@endforeach
					</tbody>
				</table>

				<form name="fechar_conta" method="POST" action="{{ url('fechar/conta', $conta->NR_CONTA) }}">
				{{ csrf_field() }}
		            <div class="elemento">
		            	<button type="submit">Fechar Conta</button>
		            </div>
		        </form>
			</div>
		</div>	
	@endif
@endforeach
	<div class="conta_mesa pedido_mesa">
	<br>
		<div class="elemento">
			<h2>Pedido da Mesa {{ $conta->NR_MESA }}</h2>
		</div>
		<div class="elemento">	
			<h5>Garçon {{ $conta->garcon->NOME }}</h5>
		</div>
		<form name="confirmar_item" method="POST" action="{{ url('/inserir/item', [$conta->NR_CONTA, $conta->NR_MESA]) }}">
        {{ csrf_field() }}
			<div class="elemento">
				<select name="NR_ITEM" data-validate="required">
	                <option value="" selected>selecione um item</option>
	                @foreach($itens_cardapio as $item)
	                    <option value="{{ $item->NR_ITEM }}">{{ $item->NOME }}</option>
	                @endforeach
	            </select>
            </div>

            <div class="elemento">
				<input type="number" name="QUANTIDADE" data-validade="required" placeholder="Quantidade">
            </div>

            <div class="elemento">
            	<button type="submit">Inserir Item</button>
            </div>
        </form>
	</div>

	<div class="conta_mesa pedido_mesa">	
		@foreach($contas_mesa as $conta)
			@if($conta->DATA == date("Y-m-d") && $conta->HORA_FECHAMENTO == null)
				<br>
				<h2>Contas da Mesa {{$conta->NR_MESA}}</h2>
				<br>
				<table cellspacing="0" cellpadding="2" border="1" class="table-relatorio">
					<thead>
						<tr>
							<th>Hora de Abertura</th>
							<th>Item do Pedido</th>
							<th>Quantidade</th>
							<th>Valor</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody  class="relatorio">
						@foreach($conta->pedido as $pedido)
							@foreach($pedido->itemCardapio as $item)
								<tr class="tdContas">
									<td>
									<input type="hidden" name="NR_CONTA" value="{{ $conta->NR_CONTA }}">
										{{ $conta->HORA_ABERTURA }}
									</td>
									<td>
										{{ $item->NOME }}
									</td>
									<td>
										{{ $pedido->QUANTIDADE }}
									</td>
									<td>
										R$ {{ number_format($pedido->PRECO_UNITARIO, 2, ',','') }}
									</td>
									<td>
										R$ {{ number_format($pedido->QUANTIDADE * $pedido->PRECO_UNITARIO, 2, ',','' )}}									
									</td>
								</tr>
							@endforeach	
						@endforeach	
					</tbody>
				</table>
				<form name="fechar_conta" method="POST" action="{{ url('fechar/conta', $conta->NR_CONTA) }}">
	        	{{ csrf_field() }}
		            <div class="elemento">
		            	<button type="submit">Fechar Conta</button>
		            </div>
		        </form>
			@endif
		@endforeach
	</div>
	@include('layouts.resultados')
@endsection