@extends ('layouts.layout')
@section ('template')
	<header>
        <div class="cabecalho">
            <a href="/"><img src="{{ asset('imagens/logo.png')}}"></a>
        </div>
    </header>
        <div class="cadastros" align="center">
	        <div class="elemento">
                <h3>Cadastrar Mesa</h3>
            </div> 
        	<form name="inserir_mesa" method="POST" action="{{ url('/mesa/inserir') }}">
			{{ csrf_field() }}
				<input type="hidden" name="NR_MESA" value="{{ $total_mesa }}">
                <br>
                <div class="elemento">
        		  <button type="submit">Inserir Mesa {{ $total_mesa }}</button>
                </div>
        	</form>

    	</div>
	
@endsection