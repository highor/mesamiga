@extends ('layouts.layout')
@section ('template')

	<header>
        <div class="cabecalho">
            <a href="/"><img src="{{ asset('imagens/logo.png')}}"></a>

            <div class="menu_cadastro">
		        <nav> 
		            <ul>
		                <li><a class="botao_menu" href="{{url('/mesa/cadastrar')}}">Mesa</a></li>
		                <li><a class="botao_menu" href="{{url('/garcon/cadastrar')}}">Garçon</a></li>
		                <li><a class="botao_menu" href="{{url('/item/cadastrar')}}">Item Cardápio</a></li>
		            </ul>
		        </nav>
	    	</div>
        </div>
    </header>
	
@endsection