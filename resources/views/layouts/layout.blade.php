<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Acesso ao Sistema</title>
		<link rel="icon" href="{{ asset('img/icon.png') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
		<link rel="stylesheet" href="{{ asset('css/sweet-alert.css') }}">
		@yield('css')
		<script src="{{ asset('js/scripts.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
		<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('js/sweet-alert.min.js') }}"></script>
		@yield('script')
	<body>
		@yield ('template')
	</body>
</html>