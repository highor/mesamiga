
@if(session('status') != '')
	<input type="hidden" value="{{ session('msg') }}" id="txtMSG">
	<input type="hidden" value="{{ session('status') }}" id="txtStatus">
	<script charset="utf-8">
		let msg = $('#txtMSG').val();
		let status = $('#txtStatus').val() == 'SUCESSO' ? 'success' : 'warning';
		window.onload = function(){
			swal({
				title: "Atenção",
				text: msg,
				type: status,
				html: true,
				confirmButtonText: "OK"
			});
		}
	</script>
@endif