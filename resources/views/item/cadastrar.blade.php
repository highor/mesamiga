@extends ('layouts.layout')
@section ('template')
    <header>
        <div class="cabecalho">
            <a href="/"><img src="{{ asset('imagens/logo.png')}}"></a>
        </div>
    </header>    
    <div class="cadastros" align="center">
        <div class="elemento">
            <h3>Cadastrar Item de Cardápio</h3>
        </div>          

        <form name="inserir_item" method="POST" action="{{ url('/item/inserir') }}">
            {{ csrf_field() }}
            <div class="elemento">
                <input type="text" name="NOME" placeholder="Nome do item" required>
            </div>
            <div class="elemento">
                <input type="text" name="DESCRICAO" placeholder="Descrição do item" required>
            </div>
            <div class="elemento">
                <input type="number" name="PRECO" placeholder="Preço do item" required>
            </div>
            <div class="elemento">
                <button type="submit">Inserir Item</button>
            </div>
        </form>
    </div>
@endsection