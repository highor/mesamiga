<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCardapio extends Model
{
    protected $table = 'ITEMCARDAPIO';
    protected $primaryKey = 'NR_ITEM';

    protected $fillable = ['NOME', 'DESCRICAO', 'PRECO'];

    public $timestamps = false;

    public function pedido()
    {
    	return $this->hasMany('App\Pedido', 'NR_ITEM', 'NR_ITEM');
    }

    public static function boot()
    {
        parent::boot();

        ItemCardapio::creating(function($item)
        {
            $ultimo_item = ItemCardapio::orderBy('NR_ITEM')->get()->last();
            if ($ultimo_item == null) {
                $item->NR_ITEM = 1;
            }
            else
            $item->NR_ITEM = $ultimo_item->NR_ITEM +1;
        });
    }
}
