<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $table = 'MESA';
    protected $primaryKey = 'NR_MESA';

    protected $fillable = ['NR_MESA'];

    public $timestamps = false;

    public function conta()
    {
    	return $this->hasMany('App\Conta', 'NR_CONTA', 'NR_MESA');
    }

    public static function boot()
    {
        parent::boot();

        Mesa::creating(function($mesa)
        {
            $ultima_mesa = Mesa::orderBy('NR_MESA')->get()->last();
            if ($ultima_mesa == null) {
                $mesa->NR_MESA = 1;
            }
            else
            $mesa->NR_MESA = $ultima_mesa->NR_MESA +1;
        });
    }
}
