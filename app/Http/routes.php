<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('mesas/', 'HomeController@mesas');
    Route::get('mesa/conta/{NR_CONTA}', 'MesaController@contaMesa');
    Route::get('vincular/garcon/{NR_MESA}', 'MesaController@contaMesa');
    Route::get('cadastros/', 'HomeController@cadastros');
    
    Route::get('mesa/cadastrar/', 'MesaController@cadastrar');
    Route::post('mesa/inserir/', 'MesaController@inserir');

    Route::get('garcon/cadastrar/', 'GarconController@cadastrar');
    Route::post('garcon/inserir/', 'GarconController@inserir');

    Route::get('item/cadastrar/', 'ItemCardapioController@cadastrar');
    Route::post('item/inserir/', 'ItemCardapioController@inserir');

    Route::post('vincular/garcon/{NR_MESA}', 'MesaController@postContaMesa');
    Route::post('/inserir/item/{NR_CONTA}/{NR_MESA}', 'MesaController@postInserirItem');
    Route::post('/fechar/conta/{NR_CONTA}', 'MesaController@postFecharConta');

});
