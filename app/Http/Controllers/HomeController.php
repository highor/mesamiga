<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mesa;

class HomeController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function cadastros()
    {
    	return view('cadastros');
    }

    public function mesas()
    {
    	$mesas = Mesa::all();
    	return view('mesa.index', ['mesas'=>$mesas]);
    }
}
