<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Log;
use App\ItemCardapio;
class ItemCardapioController extends Controller
{
    public function cadastrar()
    {
     	return view('item.cadastrar');
    }

    public function inserir(Request $request)
    {
    	$dados = $request->except('_token');

    	DB::beginTransaction();

    	try
    	{
    		$item = ItemCardapio::create($dados);
    		DB::commit();
    	}
    	catch(Exception $e)
    	{
    		DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['status' => 'ERROR', 'msg' => 'Erro ao cadastrar o item do cardápio.']);
    	}
        return redirect('/')->with(['status' => 'SUCESSO', 'msg' => 'Item do cardápio cadastrado com sucesso.']);
    }
}
