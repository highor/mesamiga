<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Conta;
use App\Garcon;
use App\ItemCardapio;
use App\Pedido;
use App\Mesa;
use DB;
use Log;

class MesaController extends Controller
{
    public function contaMesa($NR_MESA)
    {
        $data = date("Y-m-d");
        $total = 0;
        $garcons = Garcon::all();
        $contas_mesa_atrasadas = Conta::where('NR_MESA', '=', $NR_MESA)->where('HORA_FECHAMENTO', '=', null)->get();

        $contas_mesa = Conta::where('NR_MESA', '=', $NR_MESA)->where('HORA_FECHAMENTO', '=', null)->where('DATA', '=', $data)->get();

        $itens_cardapio = ItemCardapio::all();

        if (count($contas_mesa) > 0) {
            return view('mesa.conta', ['contas_mesa'=>$contas_mesa, 'garcons'=>$garcons, 'total'=>$total, 'itens_cardapio'=>$itens_cardapio, 'contas_mesa_atrasadas'=>$contas_mesa_atrasadas]);
        }
        return view('mesa.vincular_garcon', ['garcons'=>$garcons, 'mesa'=>$NR_MESA]);
        
    }

    public function postContaMesa(Request $request, $NR_MESA)
    {
        $dados = $request->except('_token');

        $dados['NR_MESA'] = $NR_MESA;

        $dados['DATA'] = date("Y-m-d");
        
        $dados['HORA_ABERTURA'] = date("H:i:s");
        
        DB::beginTransaction();
        
        try
        {
            $conta = Conta::create($dados);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('mesa/conta/'.$NR_CONTA)->with(['status' => 'ERROR', 'msg' => 'Erro ao criar conta']);
        }
        return redirect('/mesa/conta/'.$NR_MESA)->with(['status' => 'SUCESSO', 'msg' => 'Conta criada com sucesso']);
    }

    public function postInserirItem(Request $request, $NR_CONTA, $NR_MESA)
    {

        $dados = $request->except('_token');
        
        $item = ItemCardapio::find($dados['NR_ITEM']);

        $dados['PRECO_UNITARIO'] = $item->PRECO;
        $dados['NR_CONTA'] = $NR_CONTA;

        DB::beginTransaction();
        
        try
        {
            $pedido = Pedido::create($dados);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('mesa/conta/'.$NR_CONTA)->with(['status' => 'ERROR', 'msg' => 'Erro ao criar pedido']);
        }
        return redirect('/mesa/conta/'.$NR_MESA)->with(['status' => 'SUCESSO', 'msg' => 'Pedido criado com sucesso']);
    }

    public function postFecharConta($NR_CONTA)
    {

        $hora['HORA_FECHAMENTO'] = date("H:i:s");

        $conta = Conta::find($NR_CONTA);

        DB::beginTransaction();
        
        try
        {

            $conta->update(['HORA_FECHAMENTO' => $hora['HORA_FECHAMENTO']]);

            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('mesa/conta/'.$NR_CONTA)->with(['status' => 'ERROR', 'msg' => 'Erro ao fechar conta']);
        }
                
        return redirect('/mesas')->with(['status' => 'SUCESSO', 'msg' => 'Conta fechada com sucesso']);
    }

    public function cadastrar()
    {
        $mesas = Mesa::all();
        
        $total_mesa = 0;
        
        foreach ($mesas as $mesa) {
            $total_mesa = $total_mesa +1;
        }
        
        $total_mesa += 1;

        return view('mesa.cadastrar', ['total_mesa'=>$total_mesa]);
    }

    public function inserir(Request $request)
    {
        $dados = $request->except('_token');
        
        DB::beginTransaction();
        
        try
        {
            $mesa = Mesa::create($dados);

            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/mesas')->with(['status' => 'ERROR', 'msg' => 'Erro ao cadastrar Mesa']);
        }

        return redirect('/mesas')->with(['status' => 'SUCESSO', 'msg' => 'Mesa cadastrada com sucesso']);
    }
}
