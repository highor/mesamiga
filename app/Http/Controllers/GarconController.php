<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Garcon;
use DB;

class GarconController extends Controller
{
    public function cadastrar()
    {
     	return view('garcon.cadastrar');
    }

    public function inserir(Request $request)
    {
    	$dados = $request->except('_token');

    	DB::beginTransaction();

    	try
    	{
    		$garcon = Garcon::create($dados);
    		DB::commit();
    	}
    	catch(Exception $e)
    	{
    		DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['status' => 'ERROR', 'msg' => 'Erro ao cadastrar o garçon.']);
    	}
        return redirect('/')->with(['status' => 'SUCESSO', 'msg' => 'Garçon cadastrado com sucesso.']);
    }
}
