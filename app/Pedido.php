<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'PEDIDO';
    protected $primaryKey = ['NR_PEDIDO', 'NR_CONTA', 'NR_ITEM'];

    protected $fillable = ['NR_CONTA', 'NR_ITEM', 'QUANTIDADE', 'PRECO_UNITARIO'];
    
    public $incrementing = false;

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        Pedido::creating(function($pedido)
        {
            $ultimo_pedido = Pedido::orderBy('NR_PEDIDO')->get()->last();
            if ($ultimo_pedido == null) {
                $pedido->NR_PEDIDO = 1;
            }
            else
            $pedido->NR_PEDIDO = $ultimo_pedido->NR_PEDIDO +1;
        });
    }

    public function itemCardapio()
    {
    	return $this->hasMany('App\ItemCardapio', 'NR_ITEM', 'NR_ITEM');
    }

    public function conta()
    {
    	return $this->hasOne('App\Conta', 'NR_CONTA', 'NR_CONTA');
    }
}
