<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Garcon extends Model
{
    protected $table = 'GARCON';
    protected $primaryKey = 'NR_GARCON';

    protected $fillable = ['NOME'];

    public $timestamps = false;

    public function conta()
    {
    	return $this->hasMany('App\Conta', 'NR_GARCON', 'NR_GARCON');
    }

    public static function boot()
    {
        parent::boot();

        Garcon::creating(function($garcon)
        {
            $ultimo_garcon = Garcon::orderBy('NR_GARCON')->get()->last();
            if ($ultimo_garcon == null) {
                $garcon->NR_GARCON = 1;
            }
            else
            $garcon->NR_GARCON = $ultimo_garcon->NR_GARCON +1;
        });
    }
}
